docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker build -t flask-sample-one:latest .
docker run -d -p 5000:5000 flask-sample-one
docker rmi $(docker images -f "dangling=true" -q)
